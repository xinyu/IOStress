/*===================================================
Author: Evan Lin
E-mail: xinyu0123@gmail.com
====================================================*/

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

/*===================================================
| Macro
===================================================*/
#define SIZE_1K				1024
#define SIZE_1M				(1024 * 1024)

#define FILE_SIZE			(SIZE_1M * 100)

#define NUM_THREAD	    	20
#define NUM_FILE			200
#define NUM_LOOP			1000

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

/*===================================================
| Constant
===================================================*/
typedef enum {
	NO_ERROR 		= 0,
	ERROR_COMPARE 	= 1,
	ERROR_WRITE 	= 2,
	ERROR_READ		= 3,
	ERROR_STOP		= 4
}tERROR_CODE;

/*===================================================
| Structure
===================================================*/

/*===================================================
| Prototype
===================================================*/

/*===================================================
| Static Variables
===================================================*/
int forceBreak   = 0;
int globalStatus = 0;
pthread_attr_t attr;
pthread_mutex_t mutexsum;
pthread_t threads[NUM_THREAD];
struct timeval tv, preTv;
char drivePath[256];

void sig_handler(int signo)
{
	int t;
	int rc;
	int diff;    
	void* status;    

	forceBreak = 1;
	globalStatus = ERROR_STOP;
	
	for (t = 0; t < NUM_THREAD; t++)
	{
		rc = pthread_join(threads[t], &status);
	}

	gettimeofday(&tv, NULL);
	diff = tv.tv_sec - preTv.tv_sec;
	
	printf("\r\n%sTime(%2dh:%2dm:%2ds) Manual Stop!%s\n", ANSI_COLOR_BLUE, diff / 3600, (diff / 60) % 60, diff % 60, ANSI_COLOR_RESET);

	pthread_attr_destroy(&attr);
	pthread_mutex_destroy(&mutexsum);
	pthread_exit(NULL);
}

void* RunTest(void* threadid)
{
	int tid;
	int i, j;
	int file;
	int loop;
	int diff;
	int ret;
	int offset;	
	FILE* fp;
	FILE* outfp;
	char* bufS;
	char* bufD;
	char filename[256];
	
	tid = (int)threadid;
	
	bufS = calloc(1, SIZE_1M);
	bufD = calloc(1, SIZE_1M);

	for (loop = 0; loop < NUM_LOOP; loop++)
	{
		//====================================
		//printf("Loop[%2d] Thread[%3d]: Genrate Random Pattern...\n", loop, tid);

		for (i = 0; i < SIZE_1M; i++)
		{
			bufS[i] = rand() % 0xFF;
		}

		for (file = 0; file < NUM_FILE; file++)
		{
			gettimeofday(&tv, NULL);
			diff = tv.tv_sec - preTv.tv_sec;

			//================================
			sprintf(filename, "%s/%d_%d.dat", drivePath, tid, file + 1);

			pthread_mutex_lock(&mutexsum);
			if (globalStatus == NO_ERROR)
			{
				printf("\r");
				printf("Time(%2dh:%2dm:%2ds) Thread[%3d] File[%3d]: Write ", diff / 3600, (diff / 60) % 60, diff % 60, tid, file);		
			}
			pthread_mutex_unlock(&mutexsum);

			fp = fopen(filename, "wb+");
	
			for (i = 0; i < FILE_SIZE / SIZE_1M; i++)
			{
				ret = fwrite(bufS, 1, SIZE_1M, fp);

				if (ret != SIZE_1M)
				{
					globalStatus = ERROR_WRITE;

					//==================================
					gettimeofday(&tv, NULL);
					diff = tv.tv_sec - preTv.tv_sec;

					pthread_mutex_lock(&mutexsum);
					printf("\r\n%sTime(%2dh:%2dm:%2ds) Loop[%d] Thread[%d] File[%d] WRITE ERROR %s\n", ANSI_COLOR_MAGENTA, diff / 3600, (diff / 60) % 60, diff % 60, loop, tid, file, ANSI_COLOR_RESET);					
					pthread_mutex_unlock(&mutexsum);					

					forceBreak = 1;
					
					goto cmp_exit;
				}
			}	
	
			fclose(fp);
	
			//================================
			gettimeofday(&tv, NULL);
			diff = tv.tv_sec - preTv.tv_sec;

			pthread_mutex_lock(&mutexsum);
			if (globalStatus == NO_ERROR)
			{
				printf("\rTime(%2dh:%2dm:%2ds) Thread[%3d] File[%3d]: Verify", diff / 3600, (diff / 60) % 60, diff % 60, tid, file);			
			}
			pthread_mutex_unlock(&mutexsum);

			fp = fopen(filename, "rb+");
	
			for (i = 0; i < FILE_SIZE / SIZE_1M; i++)
			{
				ret = fread(bufD, 1, SIZE_1M, fp);

				if (ret != SIZE_1M)
				{
					globalStatus = ERROR_READ;

					//==================================
					gettimeofday(&tv, NULL);
					diff = tv.tv_sec - preTv.tv_sec;

					pthread_mutex_lock(&mutexsum);
					printf("\r\n%sTime(%2dh:%2dm:%2ds) Loop[%d] Thread[%d] File[%d] READ ERROR %s\n", ANSI_COLOR_YELLOW, diff / 3600, (diff / 60) % 60, diff % 60, loop, tid, file, ANSI_COLOR_RESET);
					pthread_mutex_unlock(&mutexsum);					

					forceBreak = 1;
					
					goto cmp_exit;
				}

				#if 0		
				if ((rand() % 100) == 0)
				{
					int tmp = rand() % 1000;
					bufD[tmp] |= 1;
					printf("Fake Ret[%d] i[%d]\n", tmp, i);
				}
				#endif

				ret = memcmp(bufS, bufD, SIZE_1M);

				if (ret)
				{
					globalStatus = ERROR_COMPARE;

					sprintf(filename, "%d_%d_good.dat", tid, file);

					outfp = fopen(filename, "wb+");

					fwrite(bufS, 1, SIZE_1M, outfp);

					fclose(outfp);

					//==================================
					sprintf(filename, "%d_%d_bad.dat", tid, file);

					outfp = fopen(filename, "wb+");

					fwrite(bufD, 1, SIZE_1M, outfp);

					fclose(outfp);

					//==================================
					gettimeofday(&tv, NULL);
					diff = tv.tv_sec - preTv.tv_sec;

					pthread_mutex_lock(&mutexsum);
					for (j = 0; j < SIZE_1M; j++)
					{
						if (bufS[j] != bufD[j]) 
						{
							ret = j;
							break;
						}
					}

					printf("\r\n%sTime(%2dh:%2dm:%2ds) Loop[%d] Thread[%d] File[%d] Offset[%08X] COMPARE ERROR%s\n", ANSI_COLOR_RED, diff / 3600, (diff / 60) % 60, diff % 60, loop, tid, file, i * SIZE_1M + ret, ANSI_COLOR_RESET);					

					printf("%s=== GOOD Pattern ===================", ANSI_COLOR_GREEN);					
					
					for (j = 0; j < 128; j++)
					{
						if ((j % 16) == 0)	printf("\n[%08X]:", i * SIZE_1M + ret + j);
						printf(" %02X", bufS[ret + j] & 0xFF);	
					}
					
					printf("%s\n", ANSI_COLOR_RESET);
						
					printf("%s=== BAD  Pattern ===================", ANSI_COLOR_RED);
					for (j = 0; j < 128; j++)
					{
						if ((j % 16) == 0)	printf("\n[%08X]:", i * SIZE_1M + ret + j);
						printf(" %02X", bufD[ret + j] & 0XFF);	
					}
					
					printf("%s\n", ANSI_COLOR_RESET);
					pthread_mutex_unlock(&mutexsum);				

					forceBreak = 1;
					
					goto cmp_exit;
				}
			}

			fclose(fp);

			if (forceBreak) goto cmp_exit; 
		}
	}
	
cmp_exit:
	
	//====================================
	free(bufS);
	free(bufD);

	pthread_exit(NULL);
}

/*===================================================
| MAIN FUNCTION
===================================================*/
int main(int argc, char* argv[])
{
	int rc;
	int t;
	int diff;
	void *status;
	FILE* logFp;
	time_t timer;
    char buffer[26];
    struct tm* tm_info;

	if (argc <= 1)
	{
		printf("Usage: ./IOStress [drive_path]\n");
		exit(1);
	}
	else
	{
		memcpy(drivePath, argv[1], strlen(argv[1]));
	}

	time(&timer);
    tm_info = localtime(&timer);

	signal(SIGINT, sig_handler);
	gettimeofday(&preTv, NULL);

	pthread_mutex_init(&mutexsum, NULL);
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	
	srand(time(0));

	printf("=== Configuration =========================\n");
	printf("= Total Loops    : %d\n", NUM_LOOP);
	printf("= Total Files    : %d\n", NUM_FILE * NUM_THREAD);
	printf("= Total Threads  : %d\n", NUM_THREAD);
	printf("= File Size      : %d MB \n", FILE_SIZE / SIZE_1M);
	printf("= Data Pattern   : Random\n");
	printf("= Partition Path : %s\n", drivePath);
	printf("=== Start Testing =========================\n");
	
	for (t = 0; t < NUM_THREAD; t++)
	{
		rc = pthread_create(&threads[t], NULL, RunTest, (void*)t);

		if (rc)
		{
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
	}

	for (t = 0; t < NUM_THREAD; t++)
	{
		rc = pthread_join(threads[t], &status);
		
		if (rc)
		{
			printf("ERROR; return code from pthread_join() is %d\n", rc);
		}
	}

	pthread_attr_destroy(&attr);
	pthread_mutex_destroy(&mutexsum);

	logFp = fopen("log.txt", "a+");
	
	strftime(buffer, 26, "%Y:%m:%d %H:%M:%S", tm_info);

	gettimeofday(&tv, NULL);
	diff = tv.tv_sec - preTv.tv_sec;		

	//======================================
	switch (globalStatus)
	{
		case NO_ERROR:
			fprintf(logFp, "=== Start Time:%s Testing Time:(%2dh:%2dm:%2ds) => PASS\n", buffer, diff / 3600, (diff / 60) % 60, diff % 60);
			printf("\n===%s Compare PASS %s==========================\n", ANSI_COLOR_GREEN, ANSI_COLOR_RESET);			
			break;
		case ERROR_COMPARE:
			fprintf(logFp, "=== Start Time:%s Testing Time:(%2dh:%2dm:%2ds) => Compare ERROR!\n", buffer, diff / 3600, (diff / 60) % 60, diff % 60);
			printf("\n===%s Compare ERROR %s=========================\n", ANSI_COLOR_RED, ANSI_COLOR_RESET);	
			break;
		case ERROR_WRITE:
			fprintf(logFp, "=== Start Time:%s Testing Time:(%2dh:%2dm:%2ds) => Write ERROR!\n", buffer, diff / 3600, (diff / 60) % 60, diff % 60);
			printf("\n===%s Write ERROR %s===========================\n", ANSI_COLOR_RED, ANSI_COLOR_RESET);
			break;
		case ERROR_READ:
			fprintf(logFp, "=== Start Time:%s Testing Time:(%2dh:%2dm:%2ds) => Read ERROR!\n", buffer, diff / 3600, (diff / 60) % 60, diff % 60);
			printf("\n===%s Read ERROR %s============================\n", ANSI_COLOR_RED, ANSI_COLOR_RESET);
			break;
	}

	fclose(logFp);	
		
	return 0;
}

