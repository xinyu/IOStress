CC=gcc
CFLAGS= -Wno-pointer-to-int-cast -Wno-int-to-pointer-cast
LIBS=-lpthread
OBJ = IOStress.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

IOStress: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f *.o *~ .c~
